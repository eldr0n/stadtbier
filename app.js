const fs = require('fs');
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const app = express();
const PORT = process.env.PORT || 3000;


app.use(express.static('public'));
app.use(session({secret: 'not so secret', cookie: {maxAge: 600000}}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// only for development i guess
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:63342"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.get('/products', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    fs.readFile('products.json',
        function (err, data) {
            let jsonParsed = JSON.parse(data);
            res.send(JSON.stringify(jsonParsed));
        }
    );

});

app.get('/products/:id', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    let id = req.params.id - 1;

    fs.readFile('products.json',
        function (err, data) {
            let jsonParsed = JSON.parse(data);
            res.send(JSON.stringify(jsonParsed[id]));
        }
    );
});

app.post('/products/:id/reviews', function (req, res) {

    let id = req.params.id - 1;
    let name = req.body.name;
    let mail = req.body.mail;
    let content = req.body.content;
    let stars = req.body.stars;

    fs.readFile('products.json',
        function (err, data) {
            let jsonParsed = JSON.parse(data);
            jsonParsed[id].reviews.push(
                {
                    "name": name,
                    "mail": mail,
                    "content": content,
                    "stars": parseInt(stars)
                }
            );
            let jsonContent = JSON.stringify(jsonParsed);
            //console.log(jsonParsed[id].reviews[0]);
            fs.writeFile("products.json", jsonContent, 'utf8', function (err) {
                if (err) {
                    return console.log(err);
                }
                console.log("JSON file has been saved.");
            });
        });

    res.sendStatus(200);
});


app.post('/cart', function (req, res) {
        let flag = false;
        if (req.session.name === undefined) {
            req.session.name = 'cart';
            req.session['cartJson'] = [];

        }
        if (req.session.cartJson.length !== 0) {
            for (let i = 0; i < req.session.cartJson.length; i++) {
                if (req.session.cartJson[i].product === req.body.product) {
                    req.session.cartJson[i].amount += req.body.amount;
                    flag = true;
                    res.sendStatus(200);
                }
            }
        }
        if (!flag) {
            req.session.cartJson.push({
                "product": req.body.product,
                "amount": req.body.amount,
                "price": req.body.price
            });
            res.sendStatus(200);
        }

    }
);

app.get('/cart', function (req, res) {
    if (req.session.name === undefined) {
        res.send(JSON.stringify([]));
    } else {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(req.session.cartJson));
    }
});

app.delete('/cart/product/:id', function (req, res) {
        let pos;
        let id = parseInt(req.params.id);
        for (let i = 0; i < req.session.cartJson.length; i++) {
            if (req.session.cartJson[i].product === id) {
                pos = i;
                break;
            }
        }
        req.session.cartJson.splice(pos, 1);
        res.sendStatus(200);
    }
);

app.listen(PORT);