
const cartUrl = "/cart";

function inToCart(id, price) {
    let item = {
            "product": id,
            "amount": 1,
            "price": price
        }
    ;

    fetch(cartUrl,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(item)
        })
        .then(function (res) {
            console.log(res)
        })
        .catch(function (res) {
            console.log(res)
        })
}