const cartUrl = "/cart/";
const productUrl = "/products/";

let id;
let title;
let amount;
let price;
let source;
let link;
let alt;
let total = 0;

function createSection(id, amount, price, title, source, alt, link) {
    let section = document.createElement("section");
    section.className = "inv-product";

    let divImage = document.createElement("div");
    divImage.className = "cart-image";
    let img = document.createElement("img");
    img.src = source;
    img.height = 100;
    img.alt = alt;
    divImage.appendChild(img);

    let divName = document.createElement("div");
    divName.className = "cart-pname";
    let aName = document.createElement("a");
    aName.href = link;
    aName.innerHTML = title;
    divName.appendChild(aName);

    let divAmount = document.createElement("div");
    divAmount.className = "amount";
    let formAmount = document.createElement("form");
    let inputAmount = document.createElement("input");
    inputAmount.id = "amountField" + id;
    inputAmount.type = "text";
    inputAmount.size = 1;
    inputAmount.placeholder = amount;
    let buttonRefresh = document.createElement("button");
    buttonRefresh.className = "refreshBtn";
    buttonRefresh.type = "button";
    buttonRefresh.onclick = function () {
        updateCart(id);
    };
    let iRefresh = document.createElement("i");
    iRefresh.className = "fas fa-sync-alt";
    buttonRefresh.appendChild(iRefresh);

    let buttonDelete = document.createElement("button");
    buttonDelete.className = "delBtn";
    buttonDelete.type = "button";
    buttonDelete.onclick = function () {
        deleteItem(id);
    };
    let iDelete = document.createElement("i");
    iDelete.className = "fas fa-trash-alt";
    buttonDelete.appendChild(iDelete);

    formAmount.appendChild(buttonDelete);
    formAmount.appendChild(buttonRefresh);
    formAmount.appendChild(inputAmount);
    divAmount.appendChild(formAmount);

    let divPricePer = document.createElement("div");
    divPricePer.className = "priceper";
    divPricePer.innerHTML = price + " CHF";

    let divSum = document.createElement("div");
    let sum = parseInt(amount) * parseInt(price);
    divSum.className = "sum";
    divSum.innerHTML = sum + " CHF";

    section.appendChild(divImage);
    section.appendChild(divName);
    section.appendChild(divAmount);
    section.appendChild(divPricePer);
    section.appendChild(divSum);

    document.getElementById("articles").appendChild(section);
    document.getElementById("total").innerHTML = "Total: " + total + " CHF";
}

function createEmptySec() {


    let divAlert = document.createElement("div");
    divAlert.className = "divAlert";
    divAlert.innerHTML = "Wow not so fast cowboy, es hat noch kein Produkt im Postikarra!";


    document.getElementById("articles").appendChild(divAlert);
    document.getElementById("inv-product").style.display = "none";
    document.getElementById("total").style.display = "none";
    document.getElementById("order").style.display = "none";
}

function updateCart(id) {
    let oldAmount = document.getElementById("amountField" + id).placeholder;
    let newAmount = document.getElementById("amountField" + id).value;

    let item = {
            "product": id,
            "amount": parseInt(newAmount) - parseInt(oldAmount),
        };
    console.log(item);

    fetch(cartUrl,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(item)
        })
        .then(function (res) {
            console.log(res);
            setTimeout(() => {
                window.location.reload();
            }, 750);
        })
        .catch(function (res) {
            console.log(res)
        });
}

function deleteItem(id) {
    fetch(cartUrl + "product/" + id,
        {
            method: "DELETE"
        })
    .then(function (res) {
            console.log(res);
            setTimeout(() => {
                window.location.reload();
            }, 750);
        })
    .catch(function (res) {
            console.log(res)
        });
}

fetch(cartUrl)
    .then(res => res.json())
    .then(function (cart) {
        if (cart.length === 0) {
            createEmptySec();
        } else {
            for (let i = 0; i < cart.length; i++) {
                fetch(productUrl + cart[i].product)
                    .then(res => res.json())
                    .then(function (prod) {
                        id = prod.id;
                        title = prod.title;
                        source = prod.image;
                        link = prod.link;
                        alt = prod.alt;
                        amount = cart[i].amount;
                        price = cart[i].price;
                        total = total + (amount * price);
                        if (amount !== 0) {
                            createSection(id, amount, price, title, source, alt, link);
                        }
                    });
            }
        }
    });

