let urlParams = new URLSearchParams(window.location.search);
const id = urlParams.get('id');
const n = urlParams.get('n');
const urlReview = "/products/" + id + "/reviews";

let name;
let email;
let text;
let stars;

document.getElementById("titleReview").innerHTML = "Review für " + n;
document.getElementById("sendReview").disabled = true;
document.getElementById("sendReview").addEventListener("click", function (event) {
    event.preventDefault()
});

function validateForm() {
    let re = /\S+@\S+\.\S+/; // regular expression xxxx@xxxx.xx
    name = document.getElementById('name').value;
    email = document.getElementById('mail').value;
    text = document.getElementById('content').value;

    if (name.length >= 2 && email.length > 3 && text.length > 2 && re.test(email)) {
        document.getElementById("sendReview").disabled = false;
    } else {
        document.getElementById("sendReview").disabled = true;
    }

    if(name.length >= 2) {
        let check = document.createElement("i");
        check.className= "fas fa-check";
        check.style.color = "Green";
        document.getElementById("nameinfo").innerHTML = "";
        document.getElementById("nameinfo").appendChild(check);
    }


    if(re.test(email)) {
        let check = document.createElement("i");
        check.className= "fas fa-check";
        check.style.color = "Green";
        document.getElementById("mailinfo").innerHTML = "";
        document.getElementById("mailinfo").appendChild(check);
    }

    if(text.length > 2) {
        let check = document.createElement("i");
        check.className= "fas fa-check";
        check.style.color = "green";
        document.getElementById("cominfo").innerHTML = "";
        document.getElementById("cominfo").appendChild(check);
    }
}

function sendForm() {
    stars = document.forms.reviewForm.elements.rate.value;
    let review = {
        "name": name,
        "mail": email,
        "content": text,
        "stars": parseInt(stars)
    };

    fetch(urlReview,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(review)
        })
        .then(function (res) {
            console.log(res)
        })
        .catch(function (res) {
            console.log(res)
        });

    setTimeout(() => {
        window.location = "/product.html?id=" + id;
    }, 3000);
}
