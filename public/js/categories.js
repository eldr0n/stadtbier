let urlParams = new URLSearchParams(window.location.search);
const cat = urlParams.get('cat');
const urlProducts = "/products/";

let dict = {
    helleBiere: "Helle Biere",
    seasons: "4 Seasons",
    spezialBiere: "Spezial Biere"
};

document.getElementById("prodTitel").innerHTML = dict[cat];
document.getElementById(cat).className = "nav-item nav-link active";

function createDiv(link, source, alt, title, description, id, price) {
    let div = document.createElement("div");
    div.className = "col-12 col-sm-6 col-md-5 col-lg-4" ;

    let section = document.createElement("section");
    section.className = "product";

    let aLink = document.createElement("a");
    aLink.href = link;
    let pPicture = document.createElement("p");
    pPicture.className = "prodBild";
    let image = document.createElement("img");
    image.className = "bierbild";
    image.src = source;
    image.width = 80;
    image.alt = alt;
    pPicture.appendChild(image);

    let br = document.createElement("br");
    let productTitle = document.createElement("h3");
    productTitle.innerHTML = title;

    aLink.appendChild(pPicture);
    aLink.appendChild(br);
    aLink.appendChild(productTitle);

    let pDescription = document.createElement("p");
    pDescription.className = "description";
    pDescription.innerHTML = description;

    let pPrice = document.createElement("p");
    pPrice.className = "product-price";
    pPrice.innerHTML = price + " Chf";

    let button = document.createElement("button");
    button.className = "into-inventory";
    button.setAttribute("data-toggle", "modal");
    button.setAttribute("data-target", "#exampleModal");
    button.innerHTML = "In den Einkaufswagen ";
    button.onclick = function() {
        inToCart(id, price);
    };
    let iCart = document.createElement("i");
    iCart.className = "fas fa-cart-plus";

    button.appendChild(iCart);

    section.appendChild(aLink);
    section.appendChild(pDescription);
    section.appendChild(pPrice);

    div.appendChild(section);
    div.appendChild(button);

    document.getElementById("row").appendChild(div);
}

fetch(urlProducts)
    .then(res => res.json())
    .then(function (prod) {
        for (let i = 0; i < prod.length; i++) {
            if (prod[i].category === dict[cat]) {
                let p = prod[i];
                createDiv(p.link, p.image, p.alt, p.title, p.description_short, p.id, p.price)
            }
        }
    });
