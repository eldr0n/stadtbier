const urlProduct = '/products/';

function createSlide(id, imageCarousel, alt, link, description_carousel, title) {
    let li = document.createElement("li");
    li.setAttribute("data-target", "#carouselExampleCaptions");
    li.setAttribute("data-slide-to", toString(id-1));
    if(id-1 == 0) {
        li.className = "active";
    }
    document.getElementById("ol").appendChild(li);

    let carItem = document.createElement("div");
    carItem.className = "carousel-item";
    if(id-1 == 0) {
        carItem.className = "carousel-item active";
    }
    let prodLink = document.createElement("a");
    prodLink.href = link;

    let carImg = document.createElement("img");
    carImg.className = "d-block w-100";
    carImg.src = imageCarousel;
    carImg.alt = alt;

    let carCaption = document.createElement("div");
    carCaption.className = "carousel-caption d-none d-md-block";

    let prodTitle = document.createElement("h3");
    prodTitle.innerHTML = title;
    let prodDescr = document.createElement("p");
    prodDescr.innerHTML = description_carousel;
    carCaption.appendChild(prodTitle);
    carCaption.appendChild(prodDescr);

    prodLink.appendChild(carImg);
    prodLink.appendChild(carCaption);
    carItem.appendChild(prodLink);

    document.getElementById("carousel-inner").appendChild(carItem);


}

fetch(urlProduct)
    .then(res => res.json())
    .then(function (prod) {
        for (let i = 0; i < prod.length; i++) {
                let p = prod[i];
                createSlide( p.id, p.imageCarousel, p.alt, p.link, p.description_carousel, p.title )
        }
    });