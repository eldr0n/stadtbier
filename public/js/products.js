let urlParams = new URLSearchParams(window.location.search);
const id = urlParams.get('id');
const urlProduct = '/products/' + id;

function createReview(stars, name, content) {
    let divStars = document.createElement("div");
    divStars.className = "stars";
    for (let i = 0; i < stars; i++) {
        let span = document.createElement("span");
        span.className = "fa fa-star checked";
        divStars.appendChild(span);
    }
    for (let i = 0; i < 5 - stars; i++) {
        let span = document.createElement("span");
        span.className = "fa fa-star";
        divStars.appendChild(span);
    }

    let spanName = document.createElement("span");
    spanName.className = "customer-name";
    spanName.innerHTML = " " + name;
    divStars.appendChild(spanName);

    let pComment = document.createElement("p");
    pComment.className = "comment";
    pComment.innerHTML = content;

    document.getElementById("feedback").appendChild(divStars);
    document.getElementById("feedback").appendChild(pComment);
}


fetch(urlProduct)
    .then(res => res.json())
    .then(function (data) {

        let prodImage = document.getElementById("prodImage");
        prodImage.src = data.image;

        let prodTitle = document.getElementById("prodTitle");
        prodTitle.innerHTML = data.title;

        let description = document.getElementById("description");
        description.innerHTML = data.description;

        let prodPrice = document.getElementById("prodPrice");
        prodPrice.innerHTML = data.price + " Chf";

        let buttonInv = document.getElementById("inInv");
        buttonInv.onclick = function () {
            inToCart(data.id, data.price);
        };

        if (data.reviews.length === 0) {
            let pComment = document.createElement("p");
            pComment.className = "comment";
            pComment.innerHTML = "Hoppla, es gitb noch keine Reviews :(";
            document.getElementById("feedback").appendChild(pComment);

        } else {
            for (let i = 0; i < data.reviews.length; i++) {
                let d = data.reviews[i];
                createReview(d.stars, d.name, d.content)
            }
        }

        let aLink = document.createElement("a");
        aLink.href = "review.html?id=" + id + "&n=" + data.title;

        let buttonComment = document.createElement("button");
        buttonComment.className = "write-comment";
        buttonComment.innerHTML = "Review verfassen ";

        let iComment = document.createElement("i");
        iComment.className = "far fa-comment";

        buttonComment.appendChild(iComment);
        aLink.appendChild(buttonComment);

        document.getElementById("item").appendChild(aLink);

    });


